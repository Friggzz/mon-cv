document.addEventListener("DOMContentLoaded", function(){
    const domCmdMainMenu = document.getElementById("cmd-main-menu");
    const domNavMainMenu = document.getElementById("nav-main-menu");
    
    const domCmdAvatar = document.getElementById("cmd-avatar");
    const domMaTete = document.getElementById("ma-tete")
    const domMarioLisa= document.getElementById("mario-lisa")
   
    const domCmdCompetences = document.getElementById("cmd-competences");
    const domCompetences = document.getElementById("competences");
    const domCmdExpPro = document.getElementById("cmd-exp-pro");
    const domExpPro = document.getElementById("exp-pro");
    const domCmdFormation = document.getElementById("cmd-formation");
    const domFormation = document.getElementById("formation");
    const domCmdLanguages = document.getElementById("cmd-languages");
    const domLanguages = document.getElementById("languages");
    const domCmdHobbies = document.getElementById("cmd-hobbies");
    const domHobbies = document.getElementById("hobbies");

    const domCmdMail= document.getElementById("cmd-mail");

    let boo =new Audio ("js/sound/boo.wav");
    let navi=new Audio("js/sound/navi.mp3");
    let pacman=new Audio ("js/sound/pacman_death.wav");
    let secret=new Audio("js/sound/secret.mp3");
    let tingle=new Audio("js/sound/tingle.mp3");
    let treasure=new Audio ("js/sound/WW_Secret.wav");
    let wakawaka=new Audio ("js/sound/waka-waka.mp3");

    navi.volume=0.6;
    pacman.volume=0.2;
    secret.volume=0.5;
    tingle.volume=0.3;
    treasure.volume=0.3;
    wakawaka.volume=0.5;

    domCmdAvatar.addEventListener("click", function(){
        let IsPhotoHidden = domMaTete.classList.contains("hidden");

        boo.play(onclick);

        if (IsPhotoHidden)
        {
            domMaTete.classList.remove("hidden");
            domMarioLisa.classList.add("hidden");
            
        }
        else {
            domMaTete.classList.add("hidden");
            domMarioLisa.classList.remove("hidden");
            
        }
        
    });

    domCmdMainMenu.addEventListener("click", function (){
        let IsMenuHidden = domNavMainMenu.classList.contains("hidden");

        

        if(IsMenuHidden)
        {
            pacman.play(onclick);
            domNavMainMenu.classList.remove("hidden");
            domCmdMainMenu.classList.add("close");
        }
        else {
            wakawaka.play(onclick);
            domNavMainMenu.classList.add("hidden");
            domCmdMainMenu.classList.remove("close");
        }

    })


    domCmdCompetences.addEventListener("click", function (){
        let IsCompetencesHidden = domCompetences.classList.contains("hidden");

        if(IsCompetencesHidden)
        {
            secret.play(onclick);
            domCompetences.classList.remove("hidden");
        }
        else {
            domCompetences.classList.add("hidden");
        }

    })

    domCmdExpPro.addEventListener("click", function (){
        let IsExpProHidden = domExpPro.classList.contains("hidden");

        if(IsExpProHidden)
        {
            treasure.play(onclick);
            domExpPro.classList.remove("hidden");
        }
        else {
            domExpPro.classList.add("hidden");
        }

    })

    domCmdFormation.addEventListener("click", function (){
        let IsFormationHidden = domFormation.classList.contains("hidden");

        if(IsFormationHidden)
        {
            treasure.play(onclick);
            domFormation.classList.remove("hidden");
        }
        else {
            domFormation.classList.add("hidden");
        }

    })

    domCmdLanguages.addEventListener("click", function (){
        let IsLanguagesHidden = domLanguages.classList.contains("hidden");

        if(IsLanguagesHidden)
        {
            navi.play(onclick);
            domLanguages.classList.remove("hidden");
        }
        else {
            domLanguages.classList.add("hidden");
        }
    })

    domCmdHobbies.addEventListener("click", function (){
        let IsHobbiesHidden = domHobbies.classList.contains("hidden");

        if(IsHobbiesHidden)
        {
            navi.play(onclick);
            domHobbies.classList.remove("hidden");
        }
        else {
            domHobbies.classList.add("hidden");
        }

    })

    domCmdMail.addEventListener("click", function() {
        tingle.play(onclick);
    })
});